# **Wid**

```
pip3 install pygame
```
If computer says "NO" then:-

```
sudo apt install python3-pygame
```
Or whatever, or do a venv.

**Run**
```
python3 wid.py
```
It is like swaying thingys that play sounds when they hit strings.
It is circles on stems swaying with musical breeze.

![screenshot](assets/wid_screenshot.jpg)

If you need smaller window size or your screen resolution is < 1920 x 1080
type the size -r flag the two args args. Example:-

```
python3 wid.py -r 1080 720
```
Minimum size is 450 x 300. Width must be at least 1.5 times height.

There is still room to enter silly sizes as args for a laugh.

```
python3 wid.py --help
```
options:
*  -h, --help            show this help message and exit
*  -r RESOLUTION RESOLUTION, --resolution RESOLUTION RESOLUTION
*  -s, --shuffle         Random shuffle of swinging speeds (default: False)
*  -l, --lethargic       Slowed down a bit (default: False)
