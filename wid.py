
# Copyright (C) 2024 harrysentonbury
# GNU General Public License v3.0

import argparse
import math as ma
import pygame
import random


print("===================================================")
parser = argparse.ArgumentParser(prog="wid,py",
                                 description="Mindless screen staring",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-r", "--resolution", nargs=2, dest="resolution",
                    action="store", type=int, default=[1910, 1015],
                    help="The resolution thou would like")
parser.add_argument("-s", "--shuffle", action='store_true',
                    help="Random shuffle of swinging speeds")
parser.add_argument("-l", "--lethargic", action="store_true",
                    help="Slowed down a bit")
args = parser.parse_args()


## Colors
black = (0, 0, 0)
ball_color = (200, 0, 200)
outer_ball_color = (50, 0, 50, 158)
mid_ball_color = (30, 0, 50)
line_color = (100, 100, 100)
circle_color = (25, 25, 25)

# Window
if not args.resolution:
    width, height = 1910, 1015
else:
    width, height = args.resolution[0], args.resolution[1]

if width < height:
    parser.error("Width must be >= height")
if width / height < 1.5:
    parser.error("width must be at least 1.5 times height")
if width < 300:
    parser.error("Width must be > 300")


if height < 1000:
    factor = height / 1015
else:
    factor = 1

if width < 1900:
    width_factor = width / 1910
else:
    width_factor = 1


large_circle = 75 * factor
medium_circle = 60 * factor
ball_size = 15 * factor
outer_ball_size = 90 * factor
mid_ball_size = 60 * factor
big_semi_size = 130 * factor
string_seperation = int(50 * factor)
hor_line = int(100 * factor)

# Audio
pygame.mixer.pre_init()
pygame.mixer.init(frequency=48000)
pygame.mixer.set_num_channels(12)
pygame.init()

## Low Notes
sound_Ab = pygame.mixer.Sound("audio/Ab.ogg")
sound_Ab.set_volume(0.55)
sound_Gb = pygame.mixer.Sound("audio/Gb.ogg")
sound_Gb.set_volume(0.55)
sound_D = pygame.mixer.Sound("audio/D.ogg")
sound_D.set_volume(0.55)
sound_E = pygame.mixer.Sound("audio/E.ogg")
sound_E.set_volume(0.55)
sound_El = pygame.mixer.Sound("audio/El.ogg")
sound_El.set_volume(0.55)
sound_Dl = pygame.mixer.Sound("audio/Dl.ogg")
sound_Dl.set_volume(0.55)

## High Notes
# sound_Eh = pygame.mixer.Sound("audio/a01Eh.ogg")
sound_Eh = pygame.mixer.Sound("./audio/a01Eecho.ogg")
sound_Eh.set_volume(0.55)
sound_Gbh = pygame.mixer.Sound("audio/Gbh.ogg")
sound_Gbh.set_volume(0.55)
sound_Abh = pygame.mixer.Sound("audio/Abh.ogg")
sound_Abh.set_volume(0.55)
sound_Bh = pygame.mixer.Sound("audio/Bh.ogg")
sound_Bh.set_volume(0.57)
sound_E2h = pygame.mixer.Sound("audio/Ehh.ogg")
sound_E2h.set_volume(0.59)
sound_Ab2h = pygame.mixer.Sound("audio/Abhh.ogg")
sound_Ab2h.set_volume(0.59)

window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Wid")
icon = pygame.image.load("./assets/flower_bbg_icon.png")
pygame.display.set_icon(icon)
clock = pygame.time.Clock()


def swing(mid, radius, xy, pivot_height=0, y_off=0):
    if mid[0] and mid[1] is False and mid[2] is False:
        pygame.draw.line(window, (200, 200, 200), (left_of_mid, 0),
                         (left_of_mid, height), width=3)
        return

    if mid[0] and mid[1] is False and mid[2]:
        pygame.draw.line(window, (200, 200, 200), (right_of_mid, 0),
                         (right_of_mid, height), width=3)
        return

    pygame.draw.circle(window, (30, 0, 50), (round(xy[0]),
                       round(xy[1] + y_off)), medium_circle)
    # pygame.draw.line(window, (20, 0, 10), (mid_point, pivot_height), # maybe
    #                  (xy[0], xy[1] + y_off), 4)                      #
    pygame.draw.line(window, line_color, (mid_point, pivot_height),
                     (xy[0], xy[1] + y_off), 2)
    pygame.draw.circle(window, ball_color, (round(xy[0]),
                       round(xy[1] + y_off)), ball_size)
    # pygame.draw.circle(window, (240, 0, 240), (round(xy[0] + 5), # maybe
    #                    round(xy[1] + y_off - 5)), 5)             #

    pygame.draw.circle(window, (100, 0, 100), (round(xy[0]),
                       round(xy[1] + y_off)), large_circle, width=7)
    if show_circle:
        pygame.draw.circle(window, circle_color, (mid_point,
                           height + y_off), radius, width=3)
    if mid[0]:
        pygame.draw.line(window, (200, 200, 200), (mid_point, 0),
                         (mid_point, height), width=3)


def move(theta, swing_radius, xy):
    xy[0] = mid_point + (swing_radius * ma.sin(theta))
    xy[1] = height - (swing_radius * ma.cos(theta))


def speed(theta, a, v, amount):
    a[0] = - amount * ma.sin(theta)
    v[0] += a[0]
    theta += vel[0]
    return v[0]


def hit_line(xy, hit):
    if round(xy[0]) >= mid_point - midpoint_thresh and round(xy[0])\
            <= mid_point + midpoint_thresh:
        if hit[0] is False:
            hit[0] = True
            arr.append([random.randrange(100, width - 100),
                        random.randrange(100, height - 100),
                        small_circle_count + random.randrange(-40, 40), 0])
            if xy[2] == 1:
                pygame.draw.line(window, line_color, (mid_point - hor_line, xy[1] ),
                                 (mid_point + hor_line, xy[1]), 4)
                pygame.mixer.Channel(0).play(sound_Ab)
            if xy[2] == 2:
                pygame.draw.line(window, line_color, (mid_point - hor_line, xy[1] ),
                                 (mid_point + hor_line, xy[1]), 4)
                pygame.mixer.Channel(1).play(sound_Gb)
            if xy[2] == 3:
                pygame.draw.line(window, line_color, (mid_point - hor_line, xy[1] - y_offset_1),
                                 (mid_point + hor_line, xy[1] - y_offset_1), 4)
                pygame.mixer.Channel(2).play(sound_D)
            if xy[2] == 4:
                pygame.draw.line(window, line_color, (mid_point - hor_line, xy[1] - y_offset_2),
                                 (mid_point + hor_line, xy[1] - y_offset_2), 4)
                pygame.mixer.Channel(3).play(sound_E)
            if xy[2] == 5:
                pygame.draw.line(window, line_color, (mid_point - hor_line, xy[1] ),
                                 (mid_point + hor_line, xy[1]), 4)
                pygame.mixer.Channel(4).play(sound_El)
            if xy[2] == 6:
                pygame.draw.line(window, line_color, (mid_point - hor_line, xy[1] ),
                                 (mid_point + hor_line, xy[1]), 4)
                pygame.mixer.Channel(5).play(sound_Dl)
            lines_arr.append([60])
    if round(xy[0]) < mid_point - midpoint_thresh - 1 or round(xy[0])\
            > mid_point + midpoint_thresh + 1:
        hit[0] = False



def hit_left_line(xy, hit):
    if round(xy[0]) >= left_of_mid - midpoint_thresh and round(xy[0])\
            <= left_of_mid + midpoint_thresh:
        if hit[0] is False:
            hit[0] = True
            arr.append([random.randrange(100, width - 100),
                        random.randrange(100, height - 100),
                        small_circle_count + random.randrange(-40, 40), 0])
            if xy[2] == 1:
                pygame.draw.line(window, line_color, (left_of_mid - hor_line, xy[1] ),
                                 (left_of_mid + hor_line, xy[1]), 4)
                pygame.mixer.Channel(6).play(sound_Abh)
            if xy[2] == 2:
                pygame.draw.line(window, line_color, (left_of_mid - hor_line, xy[1] ),
                                 (left_of_mid + hor_line, xy[1]), 4)
                pygame.mixer.Channel(7).play(sound_Gbh)
            if xy[2] == 3:
                pygame.draw.line(window, line_color, (left_of_mid - hor_line, xy[1] - y_offset_1),
                                 (left_of_mid + hor_line, xy[1] - y_offset_1), 4)
                pygame.mixer.Channel(8).play(sound_Bh)
            lines_arr_1.append([60])
    if round(xy[0]) < left_of_mid - midpoint_thresh - 1 or round(xy[0])\
            > left_of_mid + midpoint_thresh + 1:
        hit[0] = False


def hit_right_line(xy, hit):
    if round(xy[0]) >= right_of_mid - midpoint_thresh and round(xy[0])\
            <= right_of_mid + midpoint_thresh:
        if hit[0] is False:
            hit[0] = True
            arr.append([random.randrange(100, width - 100),
                        random.randrange(100, height - 100),
                        small_circle_count + random.randrange(-40, 40), 0])
            if xy[2] == 4:
                pygame.draw.line(window, line_color, (right_of_mid - hor_line, xy[1] - y_offset_2),
                                 (right_of_mid + hor_line, xy[1] - y_offset_2), 4)
                pygame.mixer.Channel(9).play(sound_Eh)
            if xy[2] == 5:
                pygame.draw.line(window, line_color, (right_of_mid - hor_line, xy[1] ),
                                 (right_of_mid + hor_line, xy[1]), 4)
                pygame.mixer.Channel(10).play(sound_E2h)
            if xy[2] == 6:
                pygame.draw.line(window, line_color, (right_of_mid - hor_line, xy[1] ),
                                 (right_of_mid + hor_line, xy[1]), 4)
                pygame.mixer.Channel(11).play(sound_Ab2h)
            lines_arr_2.append([60])
    if round(xy[0]) < right_of_mid - midpoint_thresh - 1 or round(xy[0])\
            > right_of_mid + midpoint_thresh + 1:
        hit[0] = False


def draw_sml_incr_circles(arrl):
    arrl[2] -= 1
    brightness = arrl[2]
    brightness = brightness if brightness < 25 else 25
    if brightness < 0:
        arr.pop(0)
    else:
        pygame.draw.circle(window, (brightness, brightness, brightness), (arrl[0],
                           arrl[1]), arrl[3], 3)
        arrl[3] += 1


def draw_line_fade(arrl):
    arrl[0] -= 1
    l_color = arrl[0]
    l_color = l_color if l_color < 50 else 50
    if l_color < 0:
        lines_arr.pop(0)
    else:
        pygame.draw.line(window, (l_color, 0, l_color), (mid_point, 0),
                         (mid_point, height), width=3)


def draw_line_1_fade(arrl):
    arrl[0] -= 1
    l_color = arrl[0]
    l_color = l_color if l_color < 50 else 50
    if l_color < 0:
        lines_arr_1.pop(0)
    else:
        pygame.draw.line(window, (l_color, 0, l_color), (left_of_mid, 0),
                         (left_of_mid, height), width=3)



def draw_line_2_fade(arrl):
    arrl[0] -= 1
    l_color = arrl[0]
    l_color = l_color if l_color < 50 else 50
    #print(l_color)
    if l_color < 0:
        lines_arr_2.pop(0)
    else:
        pygame.draw.line(window, (l_color, 0, l_color), (right_of_mid, 0),
                         (right_of_mid, height), width=3)


show_circle = False

arr = []
lines_arr = []
lines_arr_1 = []
lines_arr_2 = []

theta_0 = 0
vel = [0]
acc = [0]
theta_1 = 0
vel_1 = [0]
acc_1 = [0]
theta_2 = 0
vel_2 = [0]
acc_2 = [0]
theta_3 = 0
vel_3 = [0]
acc_3 = [0]
theta_4 = 0
vel_4 = [0]
acc_4 = [0]
theta_5 = 0
vel_5 = [0]
acc_5 = [0]

speed_amount = [0.0001, 0.0002, 0.0003, 0.0004, 0.00005 ,0.00005, 0.00003, 0.00002]
if args.shuffle:
    random.shuffle(speed_amount)
    print("Shuffled")
if args.lethargic:
    speed_amount = [round(i / 2, 6) for i in speed_amount]
    print("Lethargic")



y_offset = 0
# y_offset_1 = 100
# y_offset_2 = -100
y_offset_1 = int(100 * factor)
y_offset_2 = -int(100 * factor)
pivot_height = y_offset + height
mid_point = width // 2
small_circle_count = height // 6

midpoint_thresh = int(9 * width_factor)
hit_midpoint = [False, True, False]
hit_midpoint_1 = [False, True, False]
hit_midpoint_2 = [False, True, False]
hit_midpoint_3 = [False, True, False]
hit_midpoint_4 = [False, True, False]
hit_midpoint_5 = [False, True, False]

left_of_mid = mid_point - string_seperation
hit_midpoint_6 = [False, False, False]
hit_midpoint_7 = [False, False, False]
hit_midpoint_8 = [False, False, False]

right_of_mid = mid_point + string_seperation
hit_midpoint_9 = [False, False, True]
hit_midpoint_10 = [False, False, True]
hit_midpoint_11 = [False, False, True]

# init wid position
xy_pos = [int(220 * factor), int(500 * factor), 1]
xy_pos_1 = [int(500 * factor), int(90 * factor), 2]
xy_pos_2 = [int(300 * factor), int(120 * factor), 3]
xy_pos_3 = [int(400 * factor), int(220 * factor), 4]
xy_pos_4 = [int(350 * factor), int(190 * factor), 5]
xy_pos_5 = [int(90 * factor), int(80 * factor), 6]


def theta_radius(xy):
    swing_r= ma.sqrt(ma.pow(mid_point - xy[0], 2) + ma.pow(xy[1], 2))
    theta= ma.asin((xy[0] - mid_point) / swing_r)
    return swing_r, theta

def draw_circle_alpha(surface, color, center, radius):
    target_rect = pygame.Rect(center, (0, 0)).inflate((radius * 2, radius * 2))
    shape_surf = pygame.Surface(target_rect.size, pygame.SRCALPHA)
    pygame.draw.circle(shape_surf, color, (radius, radius), radius,
                       draw_top_left=True, draw_top_right=True)
    surface.blit(shape_surf, target_rect)

swing_radius, theta_0 = theta_radius(xy_pos)
swing_radius_1, theta_1 = theta_radius(xy_pos_1)
swing_radius_2, theta_2 = theta_radius(xy_pos_2)
swing_radius_3, theta_3 = theta_radius(xy_pos_3)
swing_radius_4, theta_4 = theta_radius(xy_pos_4)
swing_radius_5, theta_5 = theta_radius(xy_pos_5)
count = 0

run = True
while run:
    clock.tick(60)
    count += 1
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    window.fill((0, 0, 0))

    # stuff
    theta_0 += speed(theta_0, acc, vel, speed_amount[0])
    theta_1 += speed(theta_1, acc_1, vel_1, speed_amount[1])
    theta_2 += speed(theta_2, acc_2, vel_2, speed_amount[2])
    theta_3 += speed(theta_3, acc_3, vel_3, speed_amount[3])
    theta_4 += speed(theta_4, acc_4, vel_4, speed_amount[4])
    theta_5 += speed(theta_5, acc_5, vel_5, speed_amount[5])

    move(theta_0, swing_radius, xy_pos)
    move(theta_1, swing_radius_1, xy_pos_1)
    move(theta_2, swing_radius_2, xy_pos_2)
    move(theta_3, swing_radius_3, xy_pos_3)
    move(theta_4, swing_radius_4, xy_pos_4)
    move(theta_5, swing_radius_5, xy_pos_5)

    if len(arr) > 0:
        for i in arr:
            draw_sml_incr_circles(i)

    if len(lines_arr) > 0:
        for i in lines_arr:
            draw_line_fade(i)

    if len(lines_arr_1) > 0:
        for i in lines_arr_1:
            draw_line_1_fade(i)

    if len(lines_arr_2) > 0:
        for i in lines_arr_2:
            draw_line_2_fade(i)

    pygame.draw.circle(window, outer_ball_color, (mid_point, height), big_semi_size,
                       draw_top_left=True, draw_top_right=True)

    hit_line(xy_pos, hit_midpoint)
    swing(hit_midpoint, swing_radius, xy_pos, pivot_height)
    hit_line(xy_pos_1, hit_midpoint_1)
    swing(hit_midpoint_1, swing_radius_1, xy_pos_1, pivot_height)
    hit_line(xy_pos_2, hit_midpoint_2)
    swing(hit_midpoint_2, swing_radius_2, xy_pos_2, pivot_height, -y_offset_1)
    hit_line(xy_pos_3, hit_midpoint_3)
    swing(hit_midpoint_3, swing_radius_3, xy_pos_3, pivot_height, -y_offset_2)
    hit_line(xy_pos_4, hit_midpoint_4)
    swing(hit_midpoint_4, swing_radius_4, xy_pos_4, pivot_height)
    hit_line(xy_pos_5, hit_midpoint_5)
    swing(hit_midpoint_5, swing_radius_5, xy_pos_5, pivot_height)

    hit_left_line(xy_pos, hit_midpoint_6)
    swing(hit_midpoint_6, swing_radius, xy_pos, pivot_height)
    hit_left_line(xy_pos_1, hit_midpoint_7)
    swing(hit_midpoint_7, swing_radius_1, xy_pos_1, pivot_height)
    hit_left_line(xy_pos_2, hit_midpoint_8)
    swing(hit_midpoint_8, swing_radius_2, xy_pos_2, pivot_height, - y_offset_1)

    hit_right_line(xy_pos_3, hit_midpoint_9)
    swing(hit_midpoint_9, swing_radius_3, xy_pos_3, pivot_height, -y_offset_2)
    hit_right_line(xy_pos_4, hit_midpoint_10)
    swing(hit_midpoint_10, swing_radius_4, xy_pos_4, pivot_height)
    hit_right_line(xy_pos_5, hit_midpoint_11)
    swing(hit_midpoint_11, swing_radius_5, xy_pos_5, pivot_height)

    draw_circle_alpha(window, outer_ball_color, (mid_point, height), outer_ball_size)
    pygame.draw.circle(window, mid_ball_color, (mid_point, height), mid_ball_size,
                       draw_top_left=True, draw_top_right=True)

    pygame.display.flip()

